document.getElementById('more-apps-btn').addEventListener('click', () => {
    document.getElementById('more-apps-modal').style.display = 'flex';
});

document.getElementById('more-apps-modal').addEventListener('click', (e) => {
    if (e.target.id === 'more-apps-modal') {
        document.getElementById('more-apps-modal').style.display = 'none';
    }
});

// Example bookmarks
const bookmarks = [
    { name: 'Google', url: 'https://www.google.com' },
    { name: 'YouTube', url: 'https://www.youtube.com' },
    { name: 'Facebook', url: 'https://www.facebook.com' }
];

const bookmarksList = document.getElementById('bookmarks-list');
bookmarks.forEach(bookmark => {
    const li = document.createElement('li');
    const a = document.createElement('a');
    a.href = bookmark.url;
    a.textContent = bookmark.name;
    li.appendChild(a);
    bookmarksList.appendChild(li);
});
